FROM php:7.4.6-fpm-alpine3.11

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apk update && apk add --no-cache git openssh zip

RUN docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-install opcache \
    && docker-php-ext-install pdo_mysql

WORKDIR /code